	<div class="container-content">
		<div class="content">
			<div class="txt cover">
				<div class="left">
					<?php
						dynamic_sidebar("Left");
					?>
				</div>
				<div class="center">
					<?php
						dynamic_sidebar("Center");
					?>
				</div>
				<div class="right">
					<?php
						dynamic_sidebar("Right");
					?>
				</div>
			</div>
		</div>
	</div>