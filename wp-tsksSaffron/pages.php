	<div class="container-title">
		<?php
			if( have_posts() ) {
				the_post();
		?>
		<section class="title cover">
			<h1><?php the_title(); ?></h1>
		</section>
	</div>
	<div class="container-content">
		<div class="text">
			<p><?php the_content(); ?></p>
		</div>
		<?php
			}
		?>
	</div>