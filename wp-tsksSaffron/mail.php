<!DOCTYPE HTML>
<html lang="fa-IR" dir="rtl">
<head>
	<meta charset="UTF-8" />
	<title>ارسال پیام ...</title>
	<style type="text/css">
		@font-face {
			font-family:'Yekan';
			src:url('font/byekan.eot');
			src:url('font/byekan.eot?#iefix') format('embedded-opentype'),
				url('font/byekan.woff') format('woff'),
				url('font/byekan.ttf') format('truetype'),
				url('font/byekan.svg#byekan') format('svg');
			font-style:normal;
			font-weight:normal;
		}
		
		body {
			font-family:'Yekan';	
		}
	
		.ok {
			color:#23B3AB;
		}

		.err {
			color:#BD5B3D;
		}
	</style>
</head>
<body>
	<?php
		error_reporting(E_ALL ^ E_NOTICE);
		$admin = 'info@tsks.ir,farhad.raeesi@gmail.com';
		$name = $_POST['name'];
		$email = $_POST['mail'];
		$website = $_POST['url'];
		$subject = $_POST['subject'];
		$text = $_POST['txt'];

		if( strlen($name)>=3 && strlen($email)>=7 && strlen($subject)>=5 && strlen($website)>=9 && strlen($text)>=10 ){
			if( @mail (
					$admin,
					"Tsks.ir Contact : $subject",
					$text,
					"From:$name '\n' Website:$website '\n' E-mail:$email '\n' <$email>" )
			){
				echo '<h2 class="ok">پیام شما به درستی ارسال شد.</h2>';
			}else{
				echo '<h2 class="err">ارسال پیام شما با مشکل مواجه شد.</h2>';
			}
		}else{
			echo '<h2 class="err">شما اجازه دسترسی مستقیم به این صفحه را ندارید !</h2>';
		}
	?>
</body>
</html>
<!--www.zakrot.com-->