<?php
	/*
		Template Name: Products
	*/
	get_template_part('header');
	get_template_part('nav');
	get_template_part('page', 'products');
	get_template_part('footer');
?>