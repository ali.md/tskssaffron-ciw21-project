<?php
	get_template_part('header');
	get_template_part('nav');
	get_template_part('slider');
	get_template_part('box');
	get_template_part('content');
	get_template_part('footer');
?>