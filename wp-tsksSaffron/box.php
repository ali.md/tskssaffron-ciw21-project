	<div class="container-box">
		<?php
			$meta = get_post_custom();

		?>
		<section class="cover">
			<ul class="box">
				<?php
					$count_meta = count($meta['box']); // count of $meta['box']
					if( $count_meta > 0 ){
						for($i=0; $i<4; $i++){ // Show only last 4 $meta['box']
							$box = $meta['box'][$count_meta-1];
							echo "<li id='box$i'><p>$box</p></li>";
							$count_meta--;
						}
					}
				?>
			</ul>
		</section>
	</div>