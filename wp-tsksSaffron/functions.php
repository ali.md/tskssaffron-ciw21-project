<?php
	add_theme_support('menu');

	register_nav_menu('MainNav', "Main Menu"); // for header nav
	register_nav_menu('Follow', "Follow Us"); // for footer nav
	register_nav_menu('ProductTitle', "Product title"); // for Product title

	register_sidebar(array(
		'name' => 'Left',
		'id' => 'left',
		'description' => 'left text ...',
		'before_widget' => '<article class="widget %2$s">',
		'after_widget' => "</article>\n",
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => "</h2>\n"
	));
	register_sidebar(array(
		'name' => 'Center',
		'id' => 'center',
		'description' => 'center text ...',
		'before_widget' => '<article class="widget %2$s">',
		'after_widget' => "</article>\n",
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => "</h2>\n"
	));
	register_sidebar(array(
		'name' => 'Right',
		'id' => 'right',
		'description' => 'right text ...',
		'before_widget' => '<article class="widget %2$s">',
		'after_widget' => "</article>\n",
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => "</h2>\n"
	));



	add_theme_support('post-thumbnails');
	add_action('init', 'product_init');

	function product_init (){

	    $labels = array(
			'name' => 'محصولات',
			'singular_name' => 'محصول',
			'add_new' => 'افزودن محصول',
			'add_new_item' => 'افزودن محصول جدید',
			'edit_item' => 'ویرایش محصول',
			'new_item' => 'محصول جدید',
			'view_item' => 'نمایش محصول',
			'search_items' => 'جستجوی محصول',
			'not_found' => 'محصول مورد نظر یافت نشد',
			'not_found_in_trash' => 'محصول مورد نظر در زباله دان یافت نشد',
			'parent_item_colon' => 'محصول',
			'menu_name' => 'محصولات'
	    );

	    $args = array(
			'label' => 'محصولات',
	        'labels' => $labels,
	        'public' => true,
	        'publicly_queryable' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'query_var' => true,
	        'rewrite' => true,
	        'capability_type' => 'post',
	        'has_archive' => true,
	        'hierarchical' => false,
	        'menu_position' => 25,
	        'supports' => array('title','editor','thumbnail','excerpt')
	    );

	    register_post_type ('product',$args);
	    register_taxonomy_for_object_type('category', 'product');
	}





	add_action('add_meta_boxes', 'product_add_custom_box');
	add_action('save_post','product_save_meta');

	function product_add_custom_box(){
	    add_meta_box('product_priceid', 'قیمت محصول', 'product_price_box', 'product', 'side');
	}

	function product_price_box(){
	    $price = 0;
	    if ( isset($_REQUEST['post']) ) {
	        $price = get_post_meta((int)$_REQUEST['post'],'product_price',true);
	    }
	    echo "<label for='product_price_text'>قیمت محصول:</label>";
	    echo "<input id='product_price_text' class='widefat' name='product_price_text' size='20' type='text' value='$price' />";
	}

	function product_save_meta($post_id){
	    if ( is_admin() ) {
	        if ( isset($_POST['product_price_text']) ) {
	            update_post_meta($post_id,'product_price',$_POST['product_price_text']);
	        }
	    }
	}




?>