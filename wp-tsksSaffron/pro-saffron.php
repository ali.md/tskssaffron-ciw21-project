
	<div class="container-title">
		<section class="title-pro cover">
			<section class="product-title cover">
				<h1><span>محصولات ما</span> &#8194;&#8250;&#8194; <span>زعفران</span></h1>
				<nav class="cover">				
					<?php

						wp_nav_menu(array(
							'theme_location'  => 'ProductTitle',
							'menu'            => '', 
							'container'       => false, 
							'container_class' => 'menu-{menu slug}-container', 
							'container_id'    => '',
							'menu_class'      => '', 
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 0,
							'walker'          => '',
							));

					?>
				</nav>
			</section>
		</section>
	</div>
	<div class="container-product">
		<section class="product cover">
			<div class="product-link">
				<section class="pro-rows cover">
					<article class="pro-info right">
						<div class="pro-image">
				            <a href="<?php bloginfo('template_url'); ?>/images/product/saffron/pro_saffron1.jpg" title="زعفران درجه یک" rel="lightbox[group1]">
				                <img src="<?php bloginfo('template_url'); ?>/images/product/saffron/thumbnail_pro_saffron1.jpg" alt="nature1">
				            </a>
						</div>
						<div class="pro-image-info">
							<p>
								<span class="pro-name">زعفران درجه یک</span>
								<br/>
								<span class="pro-price">قیمت 22500 ریال</span>
							</p>
						</div>
						<div class="shadow"></div>
					</article>
					<article class="pro-info right mr3">
						<div class="pro-image">
				            <a href="<?php bloginfo('template_url'); ?>/images/product/saffron/pro_saffron1.jpg" title="زعفران درجه یک" rel="lightbox[group1]">
				                <img src="<?php bloginfo('template_url'); ?>/images/product/saffron/thumbnail_pro_saffron1.jpg" alt="nature1">
				            </a>
						</div>
						<div class="pro-image-info">
							<p>
								<span class="pro-name">زعفران درجه یک</span>
								<br/>
								<span class="pro-price">قیمت 22500 ریال</span>
							</p>							
						</div>
						<div class="shadow"></div>
					</article>
					<article class="pro-info mra">
						<div class="pro-image">
				            <a href="<?php bloginfo('template_url'); ?>/images/product/saffron/pro_saffron1.jpg" title="زعفران درجه یک" rel="lightbox[group1]">
				                <img src="<?php bloginfo('template_url'); ?>/images/product/saffron/thumbnail_pro_saffron1.jpg" alt="nature1">
				            </a>							
						</div>
						<div class="pro-image-info">
							<p>
								<span class="pro-name">زعفران درجه یک</span>
								<br/>
								<span class="pro-price">قیمت 22500 ریال</span>
							</p>							
						</div>
						<div class="shadow"></div>
					</article>
				</section>
				<section class="pro-rows cover">
					<article class="pro-info right">
						<div class="pro-image">
				            <a href="<?php bloginfo('template_url'); ?>/images/product/saffron/pro_saffron1.jpg" title="زعفران درجه یک" rel="lightbox[group1]">
				                <img src="<?php bloginfo('template_url'); ?>/images/product/saffron/thumbnail_pro_saffron1.jpg" alt="nature1">
				            </a>
						</div>
						<div class="pro-image-info">
							<p>
								<span class="pro-name">زعفران درجه یک</span>
								<br/>
								<span class="pro-price">قیمت 22500 ریال</span>
							</p>
						</div>
						<div class="shadow"></div>
					</article>
					<article class="pro-info right mr3">
						<div class="pro-image">
				            <a href="<?php bloginfo('template_url'); ?>/images/product/saffron/pro_saffron1.jpg" title="زعفران درجه یک" rel="lightbox[group1]">
				                <img src="<?php bloginfo('template_url'); ?>/images/product/saffron/thumbnail_pro_saffron1.jpg" alt="nature1">
				            </a>
						</div>
						<div class="pro-image-info">
							<p>
								<span class="pro-name">زعفران درجه یک</span>
								<br/>
								<span class="pro-price">قیمت 22500 ریال</span>
							</p>							
						</div>
						<div class="shadow"></div>
					</article>
					<article class="pro-info mra">
						<div class="pro-image">
				            <a href="<?php bloginfo('template_url'); ?>/images/product/saffron/pro_saffron1.jpg" title="زعفران درجه یک" rel="lightbox[group1]">
				                <img src="<?php bloginfo('template_url'); ?>/images/product/saffron/thumbnail_pro_saffron1.jpg" alt="nature1">
				            </a>							
						</div>
						<div class="pro-image-info">
							<p>
								<span class="pro-name">زعفران درجه یک</span>
								<br/>
								<span class="pro-price">قیمت 22500 ریال</span>
							</p>							
						</div>
						<div class="shadow"></div>
					</article>
				</section>
			</div>
		</section>
	</div>

