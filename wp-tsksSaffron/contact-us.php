	<div class="container-title">
		<?php
			if( have_posts() ) {
				the_post();
		?>
		<section class="title cover">
			<h1><?php the_title(); ?></h1>
		</section>
	</div>
	<div class="container-contact">
		<div class="location cover">
			<div class="address">
				<h5>نشانی ما :</h5>
				<p>مشهد مقدس / خیابان امام رضا (ع) / بین امام رضا (ع) 14 و 16 / شماره 21 / زعفران طلای سرخ خراسان</p>
				<h5>تلفن تماس :</h5>
				<p style="text-align:center;">0511-8513638<br />0915-441-3320</p>
			</div>
			<div class="map">
				<iframe width="400" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;oe=UTF8&amp;msa=0&amp;msid=218256664185939454234.0004cd47d0b758060d557&amp;t=m&amp;ll=36.278384,59.607579&amp;spn=0.003027,0.004292&amp;z=17&amp;output=embed"></iframe>
				<a href="https://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;oe=UTF8&amp;msa=0&amp;msid=218256664185939454234.0004cd47d0b758060d557&amp;t=m&amp;ll=36.278384,59.607579&amp;spn=0.003027,0.004292&amp;z=17&amp;source=embed">نمایش نقشه در اندازه اصلی</a>
			</div>
		</div>
		<div class="cover">
			<form action="<?php bloginfo('template_url'); ?>/mail.php" method="post" target="ifrm" id="contact-form">		
				<div class="right">
					<input class="text nameicon" type="text" name="name" id="name" placeholder="نام و نام خانوادگی" title="نام و نام خانوادگی" />
					<input class="text mailicon" type="text" name="mail" id="mail" placeholder="رایانامه" title="رایانامه" />
				</div>
				<div class="left">
					<input class="text urlicon" type="text" name="url" id="url" placeholder="نشانی تارنما" title="نشانی تارنما" />
					<input class="text texticon" type="text" name="subject" id="subject" placeholder="موضوع" title="موضوع" />
				</div>
				<div class="cover">				
					<div class="bottom">
						<textarea class="text texticon" name="txt" id="txt" placeholder="متن پیام" title="متن پیام" ></textarea>
						<iframe id='ifrm' name='ifrm' src="" frameborder="0" scrolling="no"></iframe>
					</div>
				</div>
				<div class="button">
					<input class="btn" type="submit" name="submit" value="ارسال" />
					<input class="btn" type="reset" name="reset" id="reset" value="دوباره" />
				</div>
			</form>
		</div>
		<div class="address">
			<p><?php the_content(); ?></p>
		</div>
		<?php
			}
		?>
	</div>