	<div class="container-title">
		<section class="title-pro cover">
			<section class="product-title cover">
				<h1><span>محصولات ما</span></h1>
				<nav class="cover">				
					<?php

						wp_nav_menu(array(
							'theme_location'  => 'ProductTitle',
							'menu'            => '', 
							'container'       => false, 
							'container_class' => 'menu-{menu slug}-container', 
							'container_id'    => '',
							'menu_class'      => '', 
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 0,
							'walker'          => '',
							));

					?>
				</nav>
			</section>
		</section>
	</div>
	<div class="container-product">
		<section class="product cover">
			<div class="product-link">
				<div class="links">
					<div class="rows pb5 cover">
						<article class="right mr3">
							<a id="saffron" href="./زعفران-2"></a>
							<h2>زعفــــــران</h2>
						</article>
						<article class="mr11">
							<a id="barberry" href="./زرشک"></a>
							<h2>زرشک</h2>
						</article>
					</div>
					<div class="rows pb5 cover">
						<article class="right mr3">
							<a id="spices" href="./ادویه-جات"></a>
							<h2>ادویه جات</h2>
						</article>
						<article class="mr11">
							<a id="dried-fruit" href="./خشکبار"></a>
							<h2>خشکبار</h2>
						</article>
					</div>							
				</div>
				<div class="product-slides">
					<div class="slides-titles s-top"></div>
					<div class="slides">
				    	<div class="slideshow cover">
				        	<div class="case-train">
				        		<div class="train">
				                    <div class="img" style="background-image:url(<?php bloginfo('template_url'); ?>/images/product/products_slider/product_s1.jpg);">
				                        <div class="info">
				                            <div>
				                                <span>زعفران سرگل</span>
				                                <span>18000 تومان</span>
				                            </div>
				                            <a href="#">اطلاعات بیشتر &#8250;&#8250;</a>
				                        </div>
				                    </div>
				                    <div class="img" style="background-image:url(<?php bloginfo('template_url'); ?>/images/product/products_slider/product_s2.jpg);">
				                        <div class="info">
				                            <div>
				                                <span>زعفران سرگل</span>
				                                <span>18000 تومان</span>
				                            </div>
				                            <a href="#">اطلاعات بیشتر &#8250;&#8250;</a>
				                        </div>
				                    </div>
				                    <div class="img" style="background-image:url(<?php bloginfo('template_url'); ?>/images/product/products_slider/product_s3.jpg);">
				                        <div class="info">
				                            <div>
				                                <span>زعفران سرگل</span>
				                                <span>18000 تومان</span>
				                            </div>
				                            <a href="#">اطلاعات بیشتر &#8250;&#8250;</a>
				                        </div>
				                    </div>
				                    <div class="img" style="background-image:url(<?php bloginfo('template_url'); ?>/images/product/products_slider/product_s4.jpg);">
				                        <div class="info">
				                            <div>
				                                <span>زعفران سرگل</span>
				                                <span>18000 تومان</span>
				                            </div>
				                            <a href="#">اطلاعات بیشتر &#8250;&#8250;</a>
				                        </div>
				                    </div>
				                    <div class="img" style="background-image:url(<?php bloginfo('template_url'); ?>/images/product/products_slider/product_s5.jpg);">
				                        <div class="info">
				                            <div>
				                                <span>زعفران سرگل</span>
				                                <span>18000 تومان</span>
				                            </div>
				                            <a href="#">اطلاعات بیشتر &#8250;&#8250;</a>
				                        </div>
				                    </div>
				          		</div>
				            </div>
				            <div class="pre"></div>
				            <div class="next"></div>
				        </div>
					</div>
					<div class="slides-titles s-bottom"></div>
				</div>
			</div>
		</section>
	</div>