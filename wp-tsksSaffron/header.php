<!DOCTYPE HTML>
<html lang="fa-IR" dir="rtl">
<head>
	<meta charset="utf-8" />
	<title><?php bloginfo('name'); wp_title(); ?></title>
	<link href="<?php bloginfo('template_url'); ?>/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<script src="<?php bloginfo('template_url'); ?>/script/html5shiv.js" type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/script/zepto.js" type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/script/script.js" type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/script/contact.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/script/jquery.js" type="text/javascript"></script> <!-- JS for Lightbox -->
    <script src="<?php bloginfo('template_url'); ?>/script/jquery-ui.js" type="text/javascript"></script> <!-- JS for Lightbox -->
    <script src="<?php bloginfo('template_url'); ?>/script/lightbox.js" type="text/javascript"></script> <!-- JS for Lightbox -->
	<script src="https://apis.google.com/js/plusone.js"></script>
	<link href="<?php bloginfo('template_url'); ?>/style/1styles.css" type="text/css" rel="stylesheet" />
	<link href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" rel="stylesheet" />
	<link href="<?php bloginfo('template_url'); ?>/product.css" type="text/css" rel="stylesheet" />
    <link href="<?php bloginfo('template_url'); ?>/style/lightbox.css" type="text/css" rel="stylesheet" media="screen" /> <!-- CSS for Lightbox -->
</head>
<body>