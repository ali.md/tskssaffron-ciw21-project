    <div class="contain-slider">
        <div class="arrow pre">
            <div class="contain-pre-nav">
                <ul class="nav-pre">
                    <li><a href="#" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/thumbnail/1.jpg)"></a></li>
                    <li><a href="#" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/thumbnail/2.jpg)"></a></li>
                    <li><a href="#" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/thumbnail/3.jpg)"></a></li>
                    <li><a href="#" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/thumbnail/4.jpg)"></a></li>
                </ul>
            </div>
        </div>
        <div class="arrow next">
            <div class="contain-next-nav">
                <ul class="nav-next">
                    <li><a href="#" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/thumbnail/1.jpg)"></a></li>
                    <li><a href="#" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/thumbnail/2.jpg)"></a></li>
                    <li><a href="#" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/thumbnail/3.jpg)"></a></li>
                    <li><a href="#" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/thumbnail/4.jpg)"></a></li>
                </ul>
            </div>
        </div>
        <div class="slider">
            <div class="train">
                <section class="s0" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/1.jpg)"></section>
                <section class="s1" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/2.jpg)"></section>
                <section class="s2" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/3.jpg)"></section>
                <section class="s3" style="background-image: url(<?php bloginfo('template_url'); ?>/images/slider/4.jpg)"></section>
            </div>
            <ul class="nav-slider">
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>
        </div>
    </div>