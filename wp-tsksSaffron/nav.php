	<div class="container-top">
		<header class="cover">
			<h1 class="logo"></h1>
			<nav>
<!-- 				<menu>
					<li><a href="#">صفحه نخست</a></li>
					<li><a href="#">درباره ما</a></li>
					<li><a href="product.html">محصولات</a></li>
					<li><a href="#">اطلاعات مفید</a></li>
					<li><a href="#">تماس با ما</a></li>
				</menu> -->
				<?php
					wp_nav_menu(array(
						'theme_location'  => 'MainNav',
						'container'       => false, 
						'container_class' => '', 
						'menu_class'      => '', 
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<menu class="%2$s">%3$s</menu>'
					));
				?>
			</nav>
		</header>
	</div>